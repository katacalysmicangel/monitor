namespace MonitOR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SensorForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OpenEvent", "Sensor_SensorID", "dbo.Sensor");
            DropForeignKey("dbo.Sensor", "OperatingRoom_OperatingRoomID", "dbo.OperatingRoom");
            DropIndex("dbo.OpenEvent", new[] { "Sensor_SensorID" });
            DropIndex("dbo.Sensor", new[] { "OperatingRoom_OperatingRoomID" });
            RenameColumn(table: "dbo.Sensor", name: "OperatingRoom_OperatingRoomID", newName: "operatingRoomID");
            DropPrimaryKey("dbo.Sensor");
            AddColumn("dbo.OpenEvent", "Sensor_operatingRoomID", c => c.Int());
            AddColumn("dbo.Sensor", "lastReset", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Sensor", "operatingRoomID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Sensor", new[] { "SensorID", "operatingRoomID" });
            CreateIndex("dbo.OpenEvent", new[] { "Sensor_SensorID", "Sensor_operatingRoomID" });
            CreateIndex("dbo.Sensor", "operatingRoomID");
            AddForeignKey("dbo.OpenEvent", new[] { "Sensor_SensorID", "Sensor_operatingRoomID" }, "dbo.Sensor", new[] { "SensorID", "operatingRoomID" });
            AddForeignKey("dbo.Sensor", "operatingRoomID", "dbo.OperatingRoom", "OperatingRoomID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sensor", "operatingRoomID", "dbo.OperatingRoom");
            DropForeignKey("dbo.OpenEvent", new[] { "Sensor_SensorID", "Sensor_operatingRoomID" }, "dbo.Sensor");
            DropIndex("dbo.Sensor", new[] { "operatingRoomID" });
            DropIndex("dbo.OpenEvent", new[] { "Sensor_SensorID", "Sensor_operatingRoomID" });
            DropPrimaryKey("dbo.Sensor");
            AlterColumn("dbo.Sensor", "operatingRoomID", c => c.Int());
            DropColumn("dbo.Sensor", "lastReset");
            DropColumn("dbo.OpenEvent", "Sensor_operatingRoomID");
            AddPrimaryKey("dbo.Sensor", "SensorID");
            RenameColumn(table: "dbo.Sensor", name: "operatingRoomID", newName: "OperatingRoom_OperatingRoomID");
            CreateIndex("dbo.Sensor", "OperatingRoom_OperatingRoomID");
            CreateIndex("dbo.OpenEvent", "Sensor_SensorID");
            AddForeignKey("dbo.Sensor", "OperatingRoom_OperatingRoomID", "dbo.OperatingRoom", "OperatingRoomID");
            AddForeignKey("dbo.OpenEvent", "Sensor_SensorID", "dbo.Sensor", "SensorID");
        }
    }
}
