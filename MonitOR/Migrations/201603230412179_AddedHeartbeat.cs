namespace MonitOR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedHeartbeat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sensor", "lastHeartbeat", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sensor", "lastHeartbeat");
        }
    }
}
