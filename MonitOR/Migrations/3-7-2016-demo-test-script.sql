﻿USE MonitorContext;

-- Operating Rooms
INSERT INTO [dbo].[OperatingRoom] ([OperatingRoomID], [Description], [Removed]) VALUES (1, N'Dr. Day''s Operating Room', 0)
INSERT INTO [dbo].[OperatingRoom] ([OperatingRoomID], [Description], [Removed]) VALUES (2, N'Dr. Richards Trauma Room', 0)
INSERT INTO [dbo].[OperatingRoom] ([OperatingRoomID], [Description], [Removed]) VALUES (3, N'Dr. Davis Operating Room', 0)
INSERT INTO [dbo].[OperatingRoom] ([OperatingRoomID], [Description], [Removed]) VALUES (4, N'Dr. Alexander Room of Pain', 0)

-- Sensors
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (1, N'1A', 0, 1, N'2016-03-07 23:22:22', 1)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (2, N'1B', 0, 1, N'2016-03-07 23:22:30', 1)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (3, N'1C', 0, 1, N'2016-03-07 23:22:53', 1)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (44, N'3A', 0, 3, N'2016-03-07 23:26:35', 0)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (45, N'3B', 0, 3, N'2016-03-07 23:26:45', 1)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (55, N'4A', 0, 4, N'2016-03-07 23:27:35', 0)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (213, N'2A', 0, 2, N'2016-03-07 23:23:36', 0)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (214, N'2B', 0, 2, N'2016-03-07 23:23:48', 1)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (215, N'2C', 0, 2, N'2016-03-07 23:26:57', 1)
INSERT INTO [dbo].[Sensor] ([SensorID], [label], [removed], [operatingRoomID], [lastReset], [active]) VALUES (216, N'2D', 0, 2, N'2016-03-07 23:27:08', 1)


-- Open Events
DECLARE @SensorId INT
DECLARE @Duration INT = 2500
DECLARE @LastResetDate DATETIME

SET @SensorId = 1
SET @LastResetDate = DATEADD(MINUTE, -10, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -9, GETDATE()), 540, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 2
SET @LastResetDate = DATEADD(MINUTE, -5, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -4, GETDATE()), 240, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 3
SET @LastResetDate = DATEADD(MINUTE, -130, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -4, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -7, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))


SET @SensorId = 44
SET @LastResetDate = DATEADD(MINUTE, -1200, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -43, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -64, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -68, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -72, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -89, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 45
SET @LastResetDate = DATEADD(MINUTE, -854, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -4, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -62, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -145, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -245, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -256, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -275, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -346, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -662, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -735, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 55
SET @LastResetDate = DATEADD(MINUTE, -512, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -52, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -67, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -113, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))


SET @SensorId = 213
SET @LastResetDate = DATEADD(MINUTE, -743, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -30, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -35, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -45, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 214
SET @LastResetDate = DATEADD(MINUTE, -245, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -24, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -36, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -73, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 215
SET @LastResetDate = DATEADD(MINUTE, -900, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -24, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -56, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -73, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

SET @SensorId = 216
SET @LastResetDate = DATEADD(MINUTE, -300, GETDATE())
UPDATE Sensor SET lastReset = @LastResetDate WHERE SensorID = @SensorId

INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -13, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -15, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -16, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -18, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))
INSERT INTO [dbo].[OpenEvent] ([time], [duration], [Sensor_SensorID], [Sensor_operatingRoomID]) VALUES (DATEADD(MINUTE, -21, GETDATE()), @Duration, @SensorId, (SELECT operatingRoomId FROM Sensor WHERE SensorId = @SensorId))

GO