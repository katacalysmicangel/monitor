namespace MonitOR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OpenEvent",
                c => new
                    {
                        OpenEventID = c.Int(nullable: false, identity: true),
                        time = c.DateTime(nullable: false),
                        duration = c.Int(nullable: false),
                        Sensor_SensorID = c.Int(),
                    })
                .PrimaryKey(t => t.OpenEventID)
                .ForeignKey("dbo.Sensor", t => t.Sensor_SensorID)
                .Index(t => t.Sensor_SensorID);
            
            CreateTable(
                "dbo.Sensor",
                c => new
                    {
                        SensorID = c.Int(nullable: false),
                        label = c.String(),
                        removed = c.Boolean(nullable: false),
                        OperatingRoom_OperatingRoomID = c.Int(),
                    })
                .PrimaryKey(t => t.SensorID)
                .ForeignKey("dbo.OperatingRoom", t => t.OperatingRoom_OperatingRoomID)
                .Index(t => t.OperatingRoom_OperatingRoomID);
            
            CreateTable(
                "dbo.OperatingRoom",
                c => new
                    {
                        OperatingRoomID = c.Int(nullable: false),
                        Description = c.String(),
                        Removed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OperatingRoomID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sensor", "OperatingRoom_OperatingRoomID", "dbo.OperatingRoom");
            DropForeignKey("dbo.OpenEvent", "Sensor_SensorID", "dbo.Sensor");
            DropIndex("dbo.Sensor", new[] { "OperatingRoom_OperatingRoomID" });
            DropIndex("dbo.OpenEvent", new[] { "Sensor_SensorID" });
            DropTable("dbo.OperatingRoom");
            DropTable("dbo.Sensor");
            DropTable("dbo.OpenEvent");
        }
    }
}
