namespace MonitOR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class activeSensors : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sensor", "active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sensor", "active");
        }
    }
}
