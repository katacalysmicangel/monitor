﻿using MonitOR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MonitOR.Controllers
{
    public class HomeController : Controller
    {
        public static ModelServices modelServices = new ModelServices();

        public ActionResult Index()
        {
            var sensorDashboards = modelServices.getAllSensorDashboards();
            return View(sensorDashboards);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult DashboardPartial(int operatingRoomID, int sensorID)
        {
            SensorDashboardViewModel dashboard = modelServices.getSensorDashboard(operatingRoomID, sensorID);
            return PartialView(dashboard);
        }
    }
}