﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MonitOR.Models;
using System.Threading.Tasks;

namespace MonitOR.Controllers
{
    public class OperatingRoomController : Controller
    {
        public static ModelServices modelServices = new ModelServices();

        //Dont think either of these are needed now. Superseded by Kat's unified OR/Sensor flow
        // GET: OperatingRoom
        /*public ActionResult Index()
        {
            List<OperatingRoomViewModel> operatingRooms = new List<OperatingRoomViewModel>();
            return View();
        }*/

        /**public ActionResult Edit(int id)
        {
            OperatingRoomViewModel model = new OperatingRoomViewModel(); //Actually needs to query to get model with specified ID
            return View(model);
        }*/

        private OperatingRoomsViewModel GetOperatingRooms()
        {
            var model = new OperatingRoomsViewModel();
            List<OperatingRoom> rooms = modelServices.getAllOperatingRooms();
            for (int i = 0; i < rooms.Count; i++)
            {
                model.OperatingRooms.Add(modelServices.OperatingRoomViewModelTransform(rooms[i]));
            }
            //Leaving these here temporarily for demo purposes
            /*model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 1, description = "OR1" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 2, description = "OR2" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 3, description = "OR3" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 4, description = "OR4" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 5, description = "OR5" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 6, description = "OR6" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 7, description = "OR7" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 8, description = "OR8" });
            model.OperatingRooms.Add(new OperatingRoomViewModel { operatingRoomID = 9, description = "OR9" });*/
            return model;
        }

        public void Delete(int id)
        {
            modelServices.removeOperatingRoom(id);
        }

        public ActionResult OperatingRooms()
        {
            return View(GetOperatingRooms());
        }

        public ActionResult OperatingRoomDetail(int id)
        {
            var model = new SensorsViewModel();
            OperatingRoom or = modelServices.getOperatingRoomByID(id);
            model.OperatingRoom = modelServices.OperatingRoomViewModelTransform(or);
            List<Sensor> sensors = or.Sensors.Where(s => s.removed == false).ToList();
            for (int i = 0; i < sensors.Count; i++ )
            {
                model.Sensors.Add(modelServices.SensorViewModelTransorm(sensors[i]));
            }

            //Leaving for Demo Purposes
            /*model.OperatingRoom = GetOperatingRooms().OperatingRooms.First(x => x.operatingRoomID == id);
            if (id == 1)
            {
                model.Sensors.Add(new SensorViewModel { label = "Sensor1A" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor1B" });
            }
            else if (id == 2)
            {
                model.Sensors.Add(new SensorViewModel { label = "Sensor2A" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor2B" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor2C" });
            }
            else if (id == 3)
            {
                model.Sensors.Add(new SensorViewModel { label = "Sensor3A" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor3B" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor3C" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor3D" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor3E" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor3F" });
            }
            else
            {
                model.Sensors.Add(new SensorViewModel { label = "Sensor1A" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor2A" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor3A" });
                model.Sensors.Add(new SensorViewModel { label = "Sensor4A" });
            }*/


            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Edit(OperatingRoomViewModel model)
        {
            if(ModelState.IsValid)
            {
                bool success = modelServices.editOperatingRoom(model);
                if(!success)
                {
                    TempData["ErrorMessage"] = "The specified OR modification could not be completed.";
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            OperatingRoomViewModel model = new OperatingRoomViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Add(OperatingRoomViewModel model)
        {
            if(ModelState.IsValid)
            {
                //Add new OR to DB
                bool success = modelServices.AddOperatingRoom(model);
                if(!success)
                {
                     TempData["ErrorMessage"] = "The OR could not be added.";
                }
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("OperatingRooms");
        }

    }
}