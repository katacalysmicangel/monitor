﻿using MonitOR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace MonitOR.Controllers
{
    public class DataController : Controller
    {
        public static ModelServices modelServices = new ModelServices();

        // GET: Data
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DownloadRaw()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult SubmitData()
        {
            //http://localhost:50221/Data/SubmitData?or_id=1&sensor_id=1&open_time=2016-03-17T22:10:32.300&seconds=2000
            try
            {
                EventViewModel model = new EventViewModel();
                model.operatingRoomID = Int32.Parse(Request.Params["or_id"]);
                model.sensorID = Int32.Parse(Request.Params["sensor_id"]);
                //model.timeStamp = new DateTime(Int64.Parse(Request.Params["open_time"]));
                model.timeStamp = Convert.ToDateTime(Request.Params["open_time"]);
                model.duration = Int32.Parse(Request.Params["seconds"]);
                if (modelServices.AddOpenEvent(model))
                {
                    //Send back 204 Success without sending any data back
                    return new HttpStatusCodeResult(HttpStatusCode.NoContent);
                }
                else
                {
                    //Send back 500
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }
            catch
            {
                //Send back 500
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public ActionResult ViewData()
        {
            var model = new SensorDataViewModel(modelServices.getAllOperatingRooms());
            return View(model);
        }

        [HttpGet]
        public FileContentResult DownloadData()
        {
            int operatingRoomID = Int32.Parse(Request.Params["operatingRoomID"]);
            int sensorID = Int32.Parse(Request.Params["sensorID"]);
            string fromDate = Request.Params["fromDate"];
            string fromTime = Request.Params["fromTime"];
            string toDate = Request.Params["toDate"];
            string toTime = Request.Params["toTime"];
            DateTime start = Convert.ToDateTime(fromDate + " " + fromTime);
            DateTime end = Convert.ToDateTime(toDate + " " + toTime);
            List<OpenEvent> events = modelServices.getEventsBySensorWithTimeConstraints(operatingRoomID, sensorID, start, end);
            string result = "Operating_Room_ID, Sensor_ID, Time, Duration" + Environment.NewLine;
            for (int i = 0; i < events.Count; i++ )
            {
                result = result + modelServices.convertOpenEventToCsvString(events[i]) + Environment.NewLine;
            }
            return File(Encoding.UTF8.GetBytes(result), "text/csv", "Event Data.csv");
        }
    }
}