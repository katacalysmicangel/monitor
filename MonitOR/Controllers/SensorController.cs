﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MonitOR.Models;
using System.Threading.Tasks;
using System.Net;

namespace MonitOR.Controllers
{
    public class SensorController : Controller
    {
        public static ModelServices modelServices = new ModelServices();

        [HttpGet]
        public ActionResult GetSensors(int operatingRoomId)
        {
            var model = new SensorDataViewModel(modelServices.getAllOperatingRooms(), modelServices.getSensorsByOperatingRoomID(operatingRoomId));
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Edit(int orID, int sensorID)
        {
            SensorViewModel sensor = modelServices.SensorViewModelTransorm(modelServices.getUniqueSensor(orID, sensorID)); 
            return View(sensor);
        }

        public ActionResult Add(int orID)
        {
            SensorViewModel sensor = new SensorViewModel();
            sensor.operatingRoomID = orID;
            return View(sensor);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Add(SensorViewModel model)
        {
            if(ModelState.IsValid)
            {
                bool success = modelServices.AddSensor(model);
                if(!success)
                {
                    TempData["ErrorMessage"] = "The sensor could not be added.";
                }
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("OperatingRooms", "OperatingRoom");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Remove(int orID, int sensorID)
        {
            bool success = modelServices.removeSensor(orID, sensorID);
            if(!success)
            {
                TempData["ErrorMessage"] = "The sensor could not be removed.";
            }
            return RedirectToAction("OperatingRooms", "OperatingRoom");
        }

        [AllowAnonymous]
        public void ResetSensor(int operatingRoomID, int sensorID)
        {
            bool success = modelServices.resetSensor(operatingRoomID, sensorID);
        }

        [AllowAnonymous]
        public ActionResult Heartbeat()
        {
            try
            {
                int orID = Int32.Parse(Request.Params["or_id"]);
                int sensorID = Int32.Parse(Request.Params["sensor_id"]);
                bool online = Boolean.Parse(Request.Params["online"]);
                if(modelServices.heartbeat(orID, sensorID, online))
                {
                    //Send back 204 Success without sending any data back
                    return new HttpStatusCodeResult(HttpStatusCode.NoContent);
                }
                else
                {
                    //Send back 500
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }
            catch
            {
                //Send back 500
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
    }
}