﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MonitOR.Startup))]
namespace MonitOR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
