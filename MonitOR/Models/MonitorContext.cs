﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MonitOR.Models
{
    public class MonitorContext : DbContext
    {
        public MonitorContext() : base("MonitorContext") { }

        public DbSet<OperatingRoom> OperatingRooms { get; set; }

        public DbSet<Sensor> Sensors { get; set; }

        public DbSet<OpenEvent> OpenEvents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}