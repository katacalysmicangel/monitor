﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitOR.Models
{
    public class ModelServices
    {
        #region OperatingRoom

        public bool AddOperatingRoom(OperatingRoomViewModel operatingRoom, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Insert into DB. Returns boolean indicating success
            try
            {
                OperatingRoom operatingRoomEntity = new OperatingRoom();
                operatingRoomEntity.OperatingRoomID = operatingRoom.operatingRoomID;
                operatingRoomEntity.Description = operatingRoom.description;
                operatingRoomEntity.Removed = false;
                operatingRoomEntity.Sensors = new List<Sensor>();
                context.OperatingRooms.Add(operatingRoomEntity);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<OperatingRoom> getAllOperatingRooms(MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Return all operating rooms currently in the DB
            List<OperatingRoom> rooms = new List<OperatingRoom>();
            rooms = (from r in context.OperatingRooms where r.Removed == false select r).ToList();
            return rooms;
        }

        public List<OperatingRoom> getOperatingRoomsByDescription(string description, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Return list of operating rooms that match given description
            List<OperatingRoom> rooms = new List<OperatingRoom>();
            rooms = (from r in context.OperatingRooms
                     where r.Description.ToLower().Contains(description.ToLower())
                     select r).ToList();
            return rooms;
        }

        public OperatingRoom getOperatingRoomByID(int id, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Get unique operating room by given id
            var room = (from r in context.OperatingRooms
                        where r.OperatingRoomID == id
                        select r);
            if(room.Any())
            {
                return room.First();
            }
            return null;
        }

        public bool editOperatingRoom(OperatingRoomViewModel changedOperatingRoom, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Change existing OR using new values from OperatingRoomViewModel. Boolean indicates success
            var rooms = (from r in context.OperatingRooms
                        where r.OperatingRoomID == changedOperatingRoom.operatingRoomID
                        select r);
            if(rooms.Any())
            {
                rooms.First().Description = changedOperatingRoom.description;
                rooms.First().Removed = changedOperatingRoom.removed;
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool removeOperatingRoom(int id, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Set removed attribute of identified Operating Room to true.  Return boolean indicating success or failure.
            try
            {
                OperatingRoom room = this.getOperatingRoomByID(id, context);
                room.Removed = true;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public OperatingRoom OperatingRoomModelTransform(OperatingRoomViewModel orView, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Transforms an OperatingRoomViewModel object into an OperatingRoom entity.
            //NOTE: MUST represent an OR already in database, NOT a new OR.
            var operatingRoom = (from r in context.OperatingRooms
                                     where r.OperatingRoomID == orView.operatingRoomID
                                     select r);
            if (operatingRoom.Any())
            {
                return operatingRoom.First();
            }
            else
            {
                return null;
            }
        }

        public OperatingRoomViewModel OperatingRoomViewModelTransform(OperatingRoom operatingRoom)
        {
            //Transform an OperatingRoom entity into an OperatingRoomViewModel object
            OperatingRoomViewModel orView = new OperatingRoomViewModel();
            orView.description = operatingRoom.Description;
            orView.operatingRoomID = operatingRoom.OperatingRoomID;
            orView.removed = operatingRoom.Removed;
            return orView;
        }

        #endregion


        #region Sensor

        public List<Sensor> getAllSensors(bool includeRemoved=true, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Return all sensors currently in the DB
            List<Sensor> sensors = new List<Sensor>();
            if (includeRemoved)
            {
                sensors = (from s in context.Sensors select s).ToList();
            }
            else
            {
                sensors = (from s in context.Sensors
                           where s.removed == false
                           select s).ToList();
            }
            return sensors;
        }

        public List<Sensor> getSensorsByOperatingRoomID(int id, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Get all sensors within a given operating room
            List<Sensor> sensors = new List<Sensor>();
            sensors = (from s in context.Sensors
                       where s.OperatingRoom.OperatingRoomID == id && s.removed == false
                       select s).ToList();
            return sensors;
        }
  
        public List<Sensor> getSensorsByLabel(string label, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Get all sensors that match the given label
            List<Sensor> sensors = new List<Sensor>();
            sensors = (from s in context.Sensors
                       where s.label.ToLower().Contains(label.ToLower())
                       select s).ToList();
            return sensors;
        }

        public Sensor getUniqueSensor(int operatingRoomID, int sensorID, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //get sensor by unique identifier operating room ID and sensor ID
            var sensor = (from s in context.Sensors
                          where s.OperatingRoom.OperatingRoomID == operatingRoomID
                          where s.SensorID == sensorID
                          select s);
            if(sensor.Any())
            {
                return sensor.First();
            }
            return null;
        }

        public bool AddSensor(SensorViewModel sensorView, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Add new sensor to DB, return boolean  indicating success or failure
            try
            {
                Sensor sensor = this.getUniqueSensor(sensorView.operatingRoomID, sensorView.sensorID, context);
                if(sensor != null)
                {
                    sensor.removed = false;
                }
                else
                {
                    sensor = new Sensor();
                    sensor.label = sensorView.label;
                    sensor.OperatingRoom = this.getOperatingRoomByID(sensorView.operatingRoomID, context);
                    sensor.operatingRoomID = sensorView.operatingRoomID;
                    sensor.SensorID = sensorView.sensorID;
                    sensor.removed = false;
                    sensor.OpenEvents = new List<OpenEvent>();
                    sensor.lastReset = DateTime.Now;
                    sensor.lastHeartbeat = DateTime.Now;
                    context.Sensors.Add(sensor);
                }
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool removeSensor(int operatingRoomID, int sensorID, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Set removed attribute of identified sensor to true.  Return boolean indicating success or failure.
            try
            {
                Sensor sensor = this.getUniqueSensor(operatingRoomID, sensorID, context);
                sensor.removed = true;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool heartbeat(int operatingRoomID, int sensorID, bool online, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            try
            {
                Sensor sensor = this.getUniqueSensor(operatingRoomID, sensorID, context);
                if (online)
                {
                    sensor.lastHeartbeat = DateTime.Now;
                    sensor.active = true;
                }
                else
                {
                    sensor.active = false;
                }
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool resetSensor(int operatingRoomID, int sensorID, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            try
            {
                Sensor sensor = this.getUniqueSensor(operatingRoomID, sensorID, context);
                sensor.lastReset = DateTime.Now;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<SensorDashboardViewModel> getAllSensorDashboards(MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            List<SensorDashboardViewModel> sensorDashboards = new List<SensorDashboardViewModel>();
            //Get all sensors
            List<Sensor> sensorModels = this.getAllSensors(false, context).OrderBy(s => s.operatingRoomID).ToList();
            DateTime now = DateTime.Now;
            for (int i = 0; i < sensorModels.Count; i++)
            {
                Sensor thisSensor = sensorModels[i];
                SensorDashboardViewModel thisDashboard = this.transformSensorToDashboard(thisSensor, context);
                sensorDashboards.Add(thisDashboard);
            }
            return sensorDashboards;
        }

        public SensorDashboardViewModel getSensorDashboard(int operatingRoomID, int sensorID, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            try
            {
                Sensor thisSensor = this.getUniqueSensor(operatingRoomID, sensorID, context);
                SensorDashboardViewModel thisDashboard = this.transformSensorToDashboard(thisSensor, context);
                return thisDashboard;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public SensorDashboardViewModel transformSensorToDashboard(Sensor thisSensor, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            try
            {
                SensorDashboardViewModel thisDashboard = new SensorDashboardViewModel();
                thisDashboard.operatingRoomID = thisSensor.OperatingRoom.OperatingRoomID;
                thisDashboard.sensorID = thisSensor.SensorID;
                thisDashboard.label = thisSensor.label;
                thisDashboard.lastReset = thisSensor.lastReset;
                //get events since last reset
                List<OpenEvent> events = (from e in this.getEventsBySensor(thisDashboard.operatingRoomID, thisDashboard.sensorID, context)
                                          where e.time > thisDashboard.lastReset
                                          orderby e.time ascending
                                          select e).ToList<OpenEvent>();
                if (events.Count > 0)
                {
                    thisDashboard.mostRecentEventTime = events[events.Count - 1].time;
                }
                else
                {
                    thisDashboard.mostRecentEventTime = new DateTime(1970, 1, 1);
                }
                int secondsOpen = 0;
                for (int j = 0; j < events.Count; j++)
                {
                    secondsOpen += events[j].duration;
                }
                double totalSeconds = (DateTime.Now - thisDashboard.lastReset).TotalSeconds;
                thisDashboard.percentOpen = (secondsOpen / totalSeconds) * 100;
                thisDashboard.openEvents = events.Count;
                thisDashboard.active = thisSensor.active;
                return thisDashboard;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public Sensor SensorModelTransform(SensorViewModel sensorView, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Transforms a SensorViewModel object into a Sensor entity.
            //NOTE: MUST represent a sensor already in database, NOT a new sensor.
            var sensor = (from s in context.Sensors
                          where s.OperatingRoom.OperatingRoomID == sensorView.operatingRoomID
                          where s.SensorID == sensorView.sensorID
                          select s);
            if (sensor.Any())
            {
                return sensor.First();
            }
            else
            {
                return null;
            }
        }

        public SensorViewModel SensorViewModelTransorm(Sensor sensor)
        {
            //Transforms a Sensor entity into a SensorViewModel
            SensorViewModel sensorView = new SensorViewModel();
            sensorView.label = sensor.label;
            sensorView.operatingRoomID = sensor.OperatingRoom.OperatingRoomID;
            sensorView.sensorID = sensor.SensorID;
            sensorView.active = sensor.active;
            var lastEvent = sensor.OpenEvents.OrderByDescending(x => x.time).FirstOrDefault();
            sensorView.mostRecentEventTime = lastEvent != null ? new DateTime?(lastEvent.time) : null;
            return sensorView;
        }


        #endregion


        #region OpenEvent

        public List<OpenEvent> getEventsBySensor(int operatingRoomID, int sensorID, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Return all events in the DB for a given sensor specified by its OR ID and sensor ID
            List<OpenEvent> events = new List<OpenEvent>();
            events = (from e in context.OpenEvents
                      where e.Sensor.OperatingRoom.OperatingRoomID == operatingRoomID
                      where e.Sensor.SensorID == sensorID
                      select e).ToList();
            return events;
        }

        public List<OpenEvent> getEventsBySensorWithTimeConstraints(int operatingRoomID, int sensorID, DateTime start, DateTime end, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            List<OpenEvent> events = new List<OpenEvent>();
            events = (from e in context.OpenEvents
                      where e.Sensor.operatingRoomID == operatingRoomID
                      where e.Sensor.SensorID == sensorID
                      where e.time >= start
                      where e.time <= end
                      select e).ToList();
            return events;
        }

        public List<OpenEvent> getAllOpenEvents(MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Get all door open events currently in DB
            List<OpenEvent> events = new List<OpenEvent>();
            events = (from e in context.OpenEvents select e).ToList();
            return events;
        }

        public bool AddOpenEvent(EventViewModel openEvent, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Add new door open event to DB. Return boolean value indicating success or failure
            try
            {
                OpenEvent dataEvent = new OpenEvent();
                dataEvent.duration = openEvent.duration;
                dataEvent.time = openEvent.timeStamp;
                dataEvent.Sensor = this.getUniqueSensor(openEvent.operatingRoomID, openEvent.sensorID, context);
                context.OpenEvents.Add(dataEvent);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public OpenEvent OpenEventModelTransform(EventViewModel eventView, MonitorContext context=null)
        {
            if (context == null)
            {
                context = new MonitorContext();
            }
            //Transforms a EventViewModel object into an OpenEvent entity.
            //NOTE: MUST represent an event already in database, NOT a new event.
            var openEvent = (from e in context.OpenEvents
                             where e.duration == eventView.duration
                             where e.Sensor.SensorID == eventView.sensorID
                             where e.Sensor.OperatingRoom.OperatingRoomID == eventView.operatingRoomID
                             select e);
            if (openEvent.Any())
            {
                return openEvent.First();
            }
            else
            {
                return null;
            }
        }

        public EventViewModel OpenEventViewModelTransorm(OpenEvent openEvent)
        {
            //Transform an OpenEvent entity into an EventViewModel object
            EventViewModel eventView = new EventViewModel();
            eventView.duration = openEvent.duration;
            eventView.operatingRoomID = openEvent.Sensor.OperatingRoom.OperatingRoomID;
            eventView.sensorID = openEvent.Sensor.SensorID;
            eventView.timeStamp = openEvent.time;
            return eventView;
        }

        public string convertOpenEventToCsvString(OpenEvent openEvent)
        {
            string result = openEvent.Sensor.operatingRoomID + "," + openEvent.Sensor.SensorID + "," + openEvent.time.ToString() + "," + openEvent.duration;
            return result;
        }

        #endregion

    }
}