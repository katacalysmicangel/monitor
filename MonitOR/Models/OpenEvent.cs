﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitOR.Models
{
    public class OpenEvent
    {
        public int OpenEventID { get; set; }

        public virtual Sensor Sensor { get; set; }

        public DateTime time { get; set; }

        public int duration { get; set; }
    }
}