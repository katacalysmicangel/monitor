﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MonitOR.Models
{
    public class HomeViewModel
    {
        public int operatingRoomID { get; set; }

        public string operatingRoomDescription { get; set; }

        public string sensorLabel { get; set; }

        public double percentOpen { get; set; } //Percent of time sensor was open in last 24 hours

        public DateTime lastOpen { get; set; } //Time of last door open event
    }

    public class OperatingRoomViewModel
    {
        [Required]
        [Display(Name = "Operating Room ID")]
        public int operatingRoomID { get; set; }

        [Display(Name="Description")]
        [Required]
        public string description { get; set; }

        public bool removed { get; set; }
    }

    public class OperatingRoomsViewModel
    {

        public OperatingRoomsViewModel()
        {
            OperatingRooms = new List<OperatingRoomViewModel>();
        }

        [Required]
        public List<OperatingRoomViewModel> OperatingRooms { get; set; }
    }

    public class SensorsViewModel
    {

        public SensorsViewModel()
        {
            Sensors = new List<SensorViewModel>();
        }

        [Required]
        public OperatingRoomViewModel OperatingRoom { get; set; }

        [Required]
        public List<SensorViewModel> Sensors { get; set; }
    }

    public class SensorDataViewModel
    {

        public SensorDataViewModel(IList<OperatingRoom> operatingRooms)
        {
            OperatingRooms = new SelectList(operatingRooms, "OperatingRoomID", "Description");
            Sensors = new SelectList(new SelectListItem[0]);
        }

        public SensorDataViewModel(IList<OperatingRoom> operatingRooms, IList<Sensor> sensors)
        {
            OperatingRooms = new SelectList(operatingRooms, "OperatingRoomID", "Description");
            Sensors = new SelectList(sensors, "SensorID", "label");
        }

        public SelectList OperatingRooms { get; set; }

        public SelectList Sensors { get; set; }

        [Required]
        public int SelectedOperatingRoomID { get; set; }

        [Required]
        public int SelectedSensorID { get; set; }

        [Required]
        public DateTime SelectedFromDate { get; set; }

        [Required]
        public DateTime SelectedToDate { get; set; }
    }

    public class SensorViewModel
    {
        [Required]
        [Display(Name = "Operating Room ID")]
        public int operatingRoomID { get; set; }

        [Required]
        [Display(Name = "Sensor ID")]
        public int sensorID { get; set; }

        [Required]
        [Display(Name = "Sensor Label")]
        public string label { get; set; }

        [Display(Name = "Most Recent Opening")]
        public DateTime? mostRecentEventTime { get; set; }

        [Display(Name = "Active")]
        public bool active { get; set; }
    }

    public class SensorDashboardViewModel
    {
        [Required]
        [Display(Name = "Operating Room ID")]
        public int operatingRoomID { get; set; }

        [Required]
        [Display(Name = "Sensor ID")]
        public int sensorID { get; set; }

        [Display(Name = "Sensor Label")]
        public string label { get; set; }

        [Display(Name = "Last Reset")]
        public DateTime lastReset { get; set; }

        [Display(Name = "Percent Time Open")]
        public double percentOpen { get; set; }

        [Display(Name = "Most Recent Opening")]
        public DateTime mostRecentEventTime { get; set; }

        [Display(Name = "Opening Count")]
        public int openEvents { get; set; }

        [Display(Name = "Active")]
        public bool active { get; set; }
    }

    public class EventViewModel
    {
        [Required]
        public int operatingRoomID { get; set; }

        [Required]
        public int sensorID { get; set; }

        [Required]
        public DateTime timeStamp { get; set; }

        [Required]
        public int duration { get; set; }
    }
}