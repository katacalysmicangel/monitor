﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MonitOR.Models
{
    public class OperatingRoom
    {
       
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OperatingRoomID { get; set; }

        public string Description { get; set; }

        public bool Removed { get; set; }

        public virtual ICollection<Sensor> Sensors { get; set; }

    }
}