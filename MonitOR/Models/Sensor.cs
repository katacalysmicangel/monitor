﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MonitOR.Models
{
    [Serializable]
    public class Sensor
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SensorID { get; set; }

        [Key, ForeignKey("OperatingRoom"), Column(Order = 1)]
        public int operatingRoomID { get; set; }

        public string label { get; set; }

        public bool removed { get; set; }

        public virtual OperatingRoom OperatingRoom { get; set; }

        public virtual ICollection<OpenEvent> OpenEvents { get; set; }

        public DateTime lastReset { get; set; }

        public bool active { get; set; }

        public DateTime lastHeartbeat { get; set; }
    }
}